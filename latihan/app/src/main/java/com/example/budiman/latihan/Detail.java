package com.example.budiman.latihan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nex3z.togglebuttongroup.button.LabelToggle;

public class Detail extends AppCompatActivity {
    TextView listdata;
    ImageView imageView;
    String nama1;
    int index;
    String aktivitas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        TextView o = (TextView) findViewById(R.id.listdata);
        ImageView image = (ImageView) findViewById(R.id.profiles);
        TextView x = (TextView) findViewById(R.id.desc);
        TextView t = (TextView) findViewById(R.id.jabatan);
        TextView y = (TextView) findViewById(R.id.aktivitas);
        Intent intent = getIntent();
        String receivedName =  intent.getStringExtra("name");
        String nim =  intent.getStringExtra("nim");
        String jabatan =  intent.getStringExtra("jabatan");
        aktivitas =  intent.getStringExtra("aktivitas");
        nama1 = intent.getStringExtra("name");
        int rImage = intent.getIntExtra("image",0);
        index = intent.getIntExtra("index",0);
        image.setImageResource(rImage);
        o.setText(receivedName);
        x.setText(nim);
        t.setText(jabatan);
        y.setText(aktivitas);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LabelToggle btnSave = findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(
                        getApplicationContext(),
                        "Data berhasil di perbaharui",
                        Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                LabelToggle x = (LabelToggle) findViewById(R.id.btnSave);
                String free = x.getText().toString();
                i.putExtra("par",1);
                i.putExtra("index1",index);
                i.putExtra("free",free);
                startActivity(i);
            }
        });
        LabelToggle btnSave1 = findViewById(R.id.btnSave1);

        btnSave1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(
                        getApplicationContext(),
                        "Data berhasil di perbaharui",
                        Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                LabelToggle x = (LabelToggle) findViewById(R.id.btnSave1);
                String free = x.getText().toString();
                i.putExtra("par",1);
                i.putExtra("index1",index);
                i.putExtra("free",free);
                startActivity(i);
            }
        });

        LabelToggle btnSave2= findViewById(R.id.btnSave2);

        btnSave2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(
                        getApplicationContext(),
                        "Data berhasil di perbaharui",
                        Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                LabelToggle x = (LabelToggle) findViewById(R.id.btnSave2);
                String free = x.getText().toString();
                i.putExtra("par",1);
                i.putExtra("index1",index);
                i.putExtra("free",free);
                startActivity(i);
            }
        });



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
