package com.example.budiman.latihan;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public static final String Extra_MESSAGE = "";
    public void sendM(View view){
        Intent intent = new Intent(this, Main2Activity.class);
        EditText edit = (EditText) findViewById(R.id.username);
        String pesan = edit.getText().toString();
        intent.putExtra(Extra_MESSAGE, pesan);
        startActivity(intent);
    }
}
