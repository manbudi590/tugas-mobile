package com.example.budiman.latihan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    private ListView lvl;
    public String[] nama ={
            "Budiman Rabbani",
            "M. Zaenudin Ahsan",
            "Aditiya Hekusa",
            "M. Ilham Darmawan",
    };
    public String[] nim ={
            "F1D016018",
            "F1D016066",
            "F1D016008",
            "F1D016056",
    };

    public String[] jabatan ={
            "Android Developer",
            "FrontEnd Develover",
            "BackEnd DEveloper",
            "Marketing",
    };
    public String[] aktivitas ={
            "Free job",
            "Free job",
            "Free job",
            "Free job",
    };

    public int[] foto ={
            R.drawable.budi,
            R.drawable.zaian,
            R.drawable.hekusa,
            R.drawable.ilham,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        lvl = findViewById(R.id.list);
        Adapter myAdapter = new Adapter();
        lvl.setAdapter(myAdapter);
        lvl.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(),Detail.class);
                i.putExtra("name",nama[position]);
                i.putExtra("image",foto[position]);
                i.putExtra("nim",nim[position]);
                i.putExtra("jabatan",jabatan[position]);
                i.putExtra("aktivitas",aktivitas[position]);
                i.putExtra("index",position);
                startActivity(i);
            }
        });
    }

    private class Adapter extends BaseAdapter {

        @Override
        public int getCount() {
            return nama.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view1 = getLayoutInflater().inflate(R.layout.row_data, null);
            //getting view in row_data
            TextView mhsname = view1.findViewById(R.id.text_View);
            ImageView imageProfil = view1.findViewById(R.id.image_View);
            Intent intent2 = getIntent();
            String updateUser = intent2.getStringExtra("free");
            int forUpdateData = intent2.getIntExtra("index1",0);
            int param = intent2.getIntExtra("par",0);

            if(param == 1){
                aktivitas[forUpdateData] = updateUser;
            }

            //setting view in row_data
            mhsname.setText(nama[position]);
            imageProfil.setImageResource(foto[position]);

            return view1;
        }
    }


}
